**Dynamic Escape Game** is a tool that given a two-player turn-based game with weighted edges allows to find the winning paths for Player 1 by considering that Player 2 can change the structure of the game. Dynamic Escape Game uses a graphical interface to build the game model and collect all winning paths for Player 1.

To execute the application : 

* Unzip the file .zip.
* Open the folder.
* Run Dynamic Escape Game.exe.

System Requirements: Microsoft Windows.